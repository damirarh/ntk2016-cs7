﻿namespace RPG.Patterns
{
    interface IWeapon
    {
        int Damage { get; set; }
    }
}
