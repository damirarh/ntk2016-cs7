﻿namespace RPG.Patterns
{
    class Bow : IWeapon
    {
        public int Damage { get; set; }
        public int Arrows { get; set; }
    }
}
