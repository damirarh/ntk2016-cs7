﻿namespace RPG.Patterns
{
    static class WeaponOperations
    {
        static void Attack(this IWeapon weapon, IEnemy enemy)
        {
            switch (weapon)
            {
                case Sword sword when sword.Durability > 0:
                    enemy.Health -= sword.Damage;
                    sword.Durability--;
                    break;
                case Bow bow when bow.Arrows > 0:
                    enemy.Health -= bow.Damage;
                    bow.Arrows--;
                    break;
            }
        }

        static void Repair(this IWeapon weapon)
        {
            if (weapon is Sword sword)
            {
                sword.Durability += 100;
            }
        }
    }
}
